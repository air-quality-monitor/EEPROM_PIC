/*
 *  Control an external 24LC256 EEPROM over I2C
 *  Definitions and function prototypes
 */

// Define EEPROM I2C pins before importing file
#define EEPROM_SDA PIN_B1
#define EEPROM_SCL PIN_B4


#define EEPROM_DATA_OFFSET 16      // 16 bytes at start of EEPROM for settings
#define EEPROM_MEASUREMENT_SIZE 8  // 8 bytes for each measurement
#define ADDR_LAST_MEASUREMENT 0    // Address where last measurement's address is stored
                                   // (takes a second address since 16 bits needed)
#define ADDR_WIPE_EEPROM 2         // Address of flag to wipe EEPROM
#define ADDR_INTERVAL 3            // Address of measurement interval


// Struct type to store measurements with timestamps
typedef struct {
  uint8_t year, month, day, hour, minute, second, PM2_5, PM10;
} Measurement;


// Function prototypes
void saveConfig(uint8_t wipe_eeprom, uint8_t measurement_interval);
void loadConfig(uint8_t &wipe_eeprom, uint8_t &measurement_interval);
void saveMeasurementAddress(uint16_t address);
uint16_t loadMeasurementAddress(void);
void saveMeasurement(Measurement &measurement);
Measurement loadMeasurement(uint16_t address);
void resetEEPROM(void);
