/*
 *  Control an external 24LC256 EEPROM over I2C
 *  Function implementations
 */

#include "eeprom.h"
#include <24256.c>


void saveConfig(uint8_t wipe_eeprom, uint8_t measurement_interval) {
  /*
   *  Save settings to the EEPROM for use by the sensor unit
   *  Delays give EEPROM time to write
   */

  write_ext_eeprom(ADDR_WIPE_EEPROM, wipe_eeprom);
  delay_ms(5);
  write_ext_eeprom(ADDR_INTERVAL, measurement_interval);
  delay_ms(5);
}


void loadConfig(uint8_t &wipe_eeprom, uint8_t &measurement_interval) {
  /*
   *  Load settings from the EEPROM
   */

  wipe_eeprom = read_ext_eeprom(ADDR_WIPE_EEPROM);
  measurement_interval = read_ext_eeprom(ADDR_INTERVAL);
}


void saveMeasurementAddress(uint16_t address) {
  /*
   *  Save the address of the last measurement to external EEPROM
   *  Uses the address defined by ADDR_LAST_MEASUREMENT
   *  (and the following one, since the address being stored is 16-bit)
   *  Needs some delays to allow the EEPROM time to write
   */

  write_ext_eeprom(ADDR_LAST_MEASUREMENT, address >> 8);      // 8 MSBs
  delay_ms(5);
  write_ext_eeprom(ADDR_LAST_MEASUREMENT+1, address & 0xFF);  // 8 LSBs
  delay_ms(5);
}


uint16_t loadMeasurementAddress(void) {
  /*
   *  Load the address of the last stored measurement from external EEPROM
   *  This address comes from ADDR_LAST_MEASUREMENT and the following byte
   */

  uint16_t address;

  address = read_ext_eeprom(ADDR_LAST_MEASUREMENT) << 8;         // 8 MSBs
  address = address | read_ext_eeprom(ADDR_LAST_MEASUREMENT+1);  // 8 LSBs

  return address;
}


void saveMeasurement(Measurement &measurement) {
  /*
   *  Save a measurement and its timestamp to the external EEPROM
   */

  // Find next address
  uint16_t address = loadMeasurementAddress();

  // Write one byte at a time, waiting for each
  write_ext_eeprom(address, measurement.year);   address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.month);  address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.day);    address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.hour);   address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.minute); address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.second); address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.PM2_5);  address++;
  delay_ms(5);
  write_ext_eeprom(address, measurement.PM10);   address++;
  delay_ms(5);

  // Store address for next write
  saveMeasurementAddress(address);
}


Measurement loadMeasurement(uint16_t address) {
  /*
   *  Load a measurement and timestamp from the external EEPROM;
   */

  // Use a pointer to each value in the struct
  // to be able to do this in a for loop
  Measurement measurement;

  measurement.year   = read_ext_eeprom(address); address++;
  measurement.month  = read_ext_eeprom(address); address++;
  measurement.day    = read_ext_eeprom(address); address++;
  measurement.hour   = read_ext_eeprom(address); address++;
  measurement.minute = read_ext_eeprom(address); address++;
  measurement.second = read_ext_eeprom(address); address++;
  measurement.PM2_5  = read_ext_eeprom(address); address++;
  measurement.PM10   = read_ext_eeprom(address); address++;

  return measurement;

}


void resetEEPROM(void) {
  /*
   *  Resets data storage by setting measurement address to the beginning
   *  Any measurements after the one pointed to by ADDR_LAST_MEASUREMENT are ignored
   */

  // Reset measurement address to the beginning
  saveMeasurementAddress(EEPROM_DATA_OFFSET);

  // Reset measurement pointed to, and reset address again
  Measurement m = {0,0,0,0,0,0,0,0};
  saveMeasurement(m);
  saveMeasurementAddress(EEPROM_DATA_OFFSET);
}
