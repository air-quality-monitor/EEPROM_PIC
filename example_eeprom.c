/*
 *  Controlling a 24LC256 EEPROM over I2C
 */

#include <16F1827.h>
#fuses MCLR,INTRC_IO,NOWDT,NOPROTECT,NOBROWNOUT,NOPUT
#use delay(clock=8000000)

#include <stdint.h>
#include <stdbool.h>


#use rs232(baud=9600, xmit=PIN_A1)



#include "eeprom.c"


void main() {
  init_ext_eeprom();

  resetEEPROM();

  Measurement m = {10, 10, 10, 10, 10, 10, 10, 10};
  saveMeasurement(m);


  printf("%Lu", loadMeasurementAddress());
  m.PM2_5 = 250;
  saveMeasurement(m);

  m = loadMeasurement(EEPROM_DATA_OFFSET);
  printf("%u", m.PM2_5);
  m = loadMeasurement(EEPROM_DATA_OFFSET+EEPROM_MEASUREMENT_SIZE);
  printf("%u", m.PM2_5);

  while(1) {
  }
}
